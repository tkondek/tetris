#include "Lshape.h"
//#include "stdafx.h"

Lshape::Lshape(Matrix *matrix, bool preview)
{
	shapeColor = { 255, 150, 0 };
	matrixPtr = matrix;
	if (preview)
	{
		initShapePreview();
	}
	else
	{
		initRegularShape();
	}
}

Lshape::~Lshape()
{
	//cout << "Lshape killed !! !! !! !!" << endl;
}

void Lshape::initShapePreview()
{
	m_rectPositions.rect1PositionX = m_previewStartPosX; //shape class field
	m_rectPositions.rect1PositionY = m_previewStartPosY; //shape class field
	m_rectPositions.rect2PositionX = m_previewStartPosX;
	m_rectPositions.rect2PositionY = m_previewStartPosY + 25;
	m_rectPositions.rect3PositionX = m_previewStartPosX;
	m_rectPositions.rect3PositionY = m_previewStartPosY + 50;
	m_rectPositions.rect4PositionX = m_previewStartPosX + 25;
	m_rectPositions.rect4PositionY = m_previewStartPosY + 50;
	SDL_Rect rect1;
	rect1.w = 24;
	rect1.h = 24;
	rect1.x = m_rectPositions.rect1PositionX;
	rect1.y = m_rectPositions.rect1PositionY;
	m_shapePreviewRects.push_back(rect1);
	SDL_Rect rect2;
	rect2.w = 24;
	rect2.h = 24;
	rect2.x = m_rectPositions.rect2PositionX;
	rect2.y = m_rectPositions.rect2PositionY;
	m_shapePreviewRects.push_back(rect2);
	SDL_Rect rect3;
	rect3.w = 24;
	rect3.h = 24;
	rect3.x = m_rectPositions.rect3PositionX;
	rect3.y = m_rectPositions.rect3PositionY;
	m_shapePreviewRects.push_back(rect3);
	SDL_Rect rect4;
	rect4.w = 24;
	rect4.h = 24;
	rect4.x = m_rectPositions.rect4PositionX;
	rect4.y = m_rectPositions.rect4PositionY;
	m_shapePreviewRects.push_back(rect4);
}

void Lshape::initRegularShape()
{
	m_rectPositions.rect1PositionX = 8;
	m_rectPositions.rect1PositionY = -3;
	m_rectPositions.rect2PositionX = 8;
	m_rectPositions.rect2PositionY = -2;
	m_rectPositions.rect3PositionX = 8;
	m_rectPositions.rect3PositionY = -1;
	m_rectPositions.rect4PositionX = 9;
	m_rectPositions.rect4PositionY = -1;
	matrixPtr->fillMatrix(m_rectPositions, FieldType::symOccupied, shapeColor);
#if DEBUG	
	matrixPtr->redrawMatrix();
#endif
}

void Lshape::rotate() 
{
	switch (m_shapePosition) 
	{
	case north:
		if (m_rectPositions.rect1PositionX - 1 < 0)
		{}
		else if (matrixPtr->getFieldType(m_rectPositions.rect1PositionX -1, m_rectPositions.rect1PositionY + 2) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect2PositionX, m_rectPositions.rect2PositionY + 1) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect3PositionX +1, m_rectPositions.rect3PositionY) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect4PositionX, m_rectPositions.rect4PositionY - 1) == FieldType::blocked)
		{}
		else 
		{
			matrixPtr->fillMatrix(m_rectPositions, FieldType::empty);
			m_rectPositions.rect1PositionX -= 1;
			m_rectPositions.rect1PositionY += 2;
			m_rectPositions.rect2PositionX += 0;
			m_rectPositions.rect2PositionY += 1;
			m_rectPositions.rect3PositionX += 1;
			m_rectPositions.rect3PositionY += 0;
			m_rectPositions.rect4PositionX += 0;
			m_rectPositions.rect4PositionY -= 1;
			matrixPtr->fillMatrix(m_rectPositions, FieldType::symOccupied, shapeColor);
#if DEBUG				
			matrixPtr->redrawMatrix();
#endif
			m_shapePosition = west;
		}
		break;
	case west:
		if (matrixPtr->getFieldType(m_rectPositions.rect1PositionX + 2, m_rectPositions.rect1PositionY) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect2PositionX + 1, m_rectPositions.rect2PositionY - 1) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect3PositionX, m_rectPositions.rect3PositionY - 2) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect4PositionX - 1, m_rectPositions.rect4PositionY - 1) == FieldType::blocked)
		{}
		else 
		{
			matrixPtr->fillMatrix(m_rectPositions, FieldType::empty);
			m_rectPositions.rect1PositionX += 2;
			m_rectPositions.rect1PositionY -= 0;
			m_rectPositions.rect2PositionX += 1;
			m_rectPositions.rect2PositionY -= 1;
			m_rectPositions.rect3PositionX -= 0;
			m_rectPositions.rect3PositionY -= 2;
			m_rectPositions.rect4PositionX -= 1;
			m_rectPositions.rect4PositionY -= 1;
			matrixPtr->fillMatrix(m_rectPositions, FieldType::symOccupied, shapeColor);
#if DEBUG				
			matrixPtr->redrawMatrix();
#endif		
			m_shapePosition = south;
		}
		break;
	case south:
		if (m_rectPositions.rect3PositionX - 2 < 0) 
		{}
		else if (matrixPtr->getFieldType(m_rectPositions.rect1PositionX, m_rectPositions.rect1PositionY - 1) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect2PositionX - 1, m_rectPositions.rect2PositionY) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect3PositionX - 2, m_rectPositions.rect3PositionY + 1) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect4PositionX - 1, m_rectPositions.rect4PositionY + 2) == FieldType::blocked)
		{}
		else 
		{
			matrixPtr->fillMatrix(m_rectPositions, FieldType::empty);
			m_rectPositions.rect1PositionX -= 0;
			m_rectPositions.rect1PositionY -= 1;
			m_rectPositions.rect2PositionX -= 1;
			m_rectPositions.rect2PositionY -= 0;
			m_rectPositions.rect3PositionX -= 2;
			m_rectPositions.rect3PositionY += 1;
			m_rectPositions.rect4PositionX -= 1;
			m_rectPositions.rect4PositionY += 2;
			matrixPtr->fillMatrix(m_rectPositions, FieldType::symOccupied, shapeColor);
#if DEBUG				
			matrixPtr->redrawMatrix();
#endif		
			m_shapePosition = east;
		}
		break;
	case east:
		if (matrixPtr->getFieldType(m_rectPositions.rect1PositionX - 1, m_rectPositions.rect1PositionY - 1) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect2PositionX, m_rectPositions.rect2PositionY) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect3PositionX + 1, m_rectPositions.rect3PositionY + 1) == FieldType::blocked ||
			matrixPtr->getFieldType(m_rectPositions.rect4PositionX + 2, m_rectPositions.rect4PositionY) == FieldType::blocked)
		{}
		else 
		{
			matrixPtr->fillMatrix(m_rectPositions, FieldType::empty);
			m_rectPositions.rect1PositionX -= 1;
			m_rectPositions.rect1PositionY -= 1;
			m_rectPositions.rect2PositionX -= 0;
			m_rectPositions.rect2PositionY -= 0;
			m_rectPositions.rect3PositionX += 1;
			m_rectPositions.rect3PositionY += 1;
			m_rectPositions.rect4PositionX += 2;
			m_rectPositions.rect4PositionY -= 0;
			matrixPtr->fillMatrix(m_rectPositions, FieldType::symOccupied, shapeColor);
#if DEBUG				
			matrixPtr->redrawMatrix();
#endif
			m_shapePosition = north;
		}
		break;
	default:
		break;
	}
}