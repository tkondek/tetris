#pragma once
#include "shape.h"

class Ishape :public Shape {

public:
	Ishape(Matrix *matrix, bool m_preview);
	~Ishape();
	void initShapePreview();
	void initRegularShape();
	virtual void rotate();
};