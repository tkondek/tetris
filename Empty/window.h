#pragma once
#include "stdafx.h"
#include "matrix.h"
#include "settings.h"

enum class FieldType : char;

class Window {
public:
	Window(const std::string &title, Settings *settings);
	~Window();
	void clear() const;
	void renderBackground();
	inline bool isClosed() const { return windowClosed; }
	SDL_Renderer *m_rendererPtr = nullptr;
	bool windowClosed = false;
	int windWidth;
	int windHeight;
	int gameFieldWidth;
	int sideFieldWidth;

private:
	bool init();
	bool loadBackgroundImg(std::string bmpName);
	void initBackground();
	std::string m_title;
	SDL_Window *m_window = nullptr;
	SDL_Surface *backgroundSurface = nullptr;
	SDL_Texture *backgroundTexture = nullptr;
};
