#include "gameFlow.h"
#include "random"
#include "label.h"
#include "Lshape.h"
#include "Rshape.h"
#include "Ishape.h"
#include "Jshape.h"
#include "Zshape.h"
#include "Sshape.h"
#include "Tshape.h"

GameFlow::GameFlow(Window *window, Settings *settings)
{
	m_windowPtr = window;
	m_gameMenuPtr = std::make_unique <GameMenu> (m_windowPtr->m_rendererPtr, settings);
	m_gameTimerPtr = std::make_unique <Timer>();
	m_promptTimerPtr = std::make_unique <Timer>();
	m_matrixPtr = std::make_shared <Matrix>();
	m_scoreMgrPtr = std::make_unique <ScoreManager>(m_windowPtr->m_rendererPtr, settings);
	m_speedMultiplier = static_cast<GameSpeed>(settings->getSpeedMultiplier()); //possibly illegal, I'm not sure :/
	initLabels();
}

void GameFlow::playGame() 
{
	SDL_Event event;
	m_gameTimerPtr->setStartTime();
	m_promptTimerPtr->setStartTime();
	while (!m_windowPtr->isClosed())
	{
		eventHandler(event);
		m_gameTimerPtr->getTime();
		m_windowPtr->renderBackground();
		if (!m_gameMenuPtr->m_isMenuPresented) 
		{
			if (m_currentShapePtr == nullptr) 
			{
				m_currentShapePtr = initShape(randomNumber(), false);
				m_nextShapePtr = initShape(randomNumber(), true);
			}
			if (m_gameTimerPtr->isNextFrame(m_currentGameSpeed-(m_speedMultiplier *100)) && !m_gameOver)
			{
				
				if (!m_currentShapePtr->m_blockedShape) 
				{
					m_currentShapePtr->move(Direction::down);
				}
				else 
				{
					bool fullLines = m_matrixPtr->eraseFullLines();
					if (fullLines)
					{
						m_erasedLines++;
						m_gameState = GameState::lineErasing;
						m_currentGameSpeed = 600; //in miliseconds, delay to next shape move
					}
					else
					{
						m_gameState = GameState::started;
						m_currentGameSpeed = 300; //in miliseconds, delay to next shape move
						increaseScore(m_erasedLines);
						m_erasedLines = 0;
					}
					if (m_gameState != GameState::lineErasing)
					{
						m_currentShapePtr = nullptr;
						m_currentShapePtr = std::move(m_nextShapePtr);
						m_currentShapePtr->initRegularShape();
						m_nextShapePtr = initShape(randomNumber(), true);
					}
				}
			}
			if (canPlayCurrentGame()) 
			{
				drawGame();
			}
			else
			{
				if (m_gameState == GameState::started)
				{
					m_scoreMgrPtr->checkAndSaveScore(m_gameMenuPtr->m_playerName, m_scoreMgrPtr->getCurrentScore());
					finishCurrentGame();
					m_gameState = GameState::scoreScreenAfterGame;
				}
				if (m_gameState == GameState::scoreScreenAfterGame)
				{
					drawGame();
					drawScoreScreen();
				}
			}
		}
		else 
		{
			drawMenu();
			if (m_gameState == GameState::scoreScreenMenu)
			{
				drawScoreScreen();
			}
		}
		m_windowPtr->clear();
	}
}

void GameFlow::initLabels()
{
	m_nextShapeLblPtr = std::make_unique <Label>("| Next Shape |", m_labelDftColor, m_windowPtr->m_rendererPtr, "arial.ttf", 18);
	int nextShapeLblPosX = m_windowPtr->gameFieldWidth + (m_windowPtr->sideFieldWidth / 2) - (m_nextShapeLblPtr->m_textRect.w / 2);
	int nextShapeLblPosY = 10;
	m_nextShapeLblPtr->setTextPosition(nextShapeLblPosX, nextShapeLblPosY);

	m_scoreLblPtr = std::make_unique <Label>("| SCORE | ", m_labelDftColor, m_windowPtr->m_rendererPtr, "arial.ttf", 18);
	int scoreLblPosX = m_windowPtr->gameFieldWidth + (m_windowPtr->sideFieldWidth / 2) - (m_scoreLblPtr->m_textRect.w / 2);
	int scoreLblPosY = 520;
	m_scoreLblPtr->setTextPosition(scoreLblPosX,scoreLblPosY);
	
	m_scoreCounterLblPtr = std::make_unique <Label>(std::to_string(m_scoreMgrPtr->getCurrentScore()), m_labelDftColor, m_windowPtr->m_rendererPtr, "arial.ttf", 18);
	int scoreCounterLblPosX = m_windowPtr->gameFieldWidth + (m_windowPtr->sideFieldWidth / 2) - (m_scoreCounterLblPtr->m_textRect.w / 2);
	int scoreCounterLblPosY = (m_scoreLblPtr->m_textRect.y) + (m_scoreLblPtr->m_textRect.h) + 5;
	m_scoreCounterLblPtr->setTextPosition(scoreCounterLblPosX, scoreCounterLblPosY);

	m_multiplicator1LblPtr = std::make_unique <Label>("! x1 !", m_labelDftColor, m_windowPtr->m_rendererPtr, "arial.ttf", 40);
	int multiplicator1LblPosX = (m_windowPtr->gameFieldWidth / 2) - (m_multiplicator1LblPtr->m_textRect.w / 2);
	int multiplicator1LblPosY = (m_windowPtr->windHeight / 2) - (m_multiplicator1LblPtr->m_textRect.h / 2);
	m_multiplicator1LblPtr->setTextPosition(multiplicator1LblPosX, multiplicator1LblPosY);

	m_multiplicator2LblPtr = std::make_unique <Label>("! x2 !", m_labelDftColor, m_windowPtr->m_rendererPtr, "arial.ttf", 50);
	int multiplicator2LblPosX = (m_windowPtr->gameFieldWidth / 2) - (m_multiplicator2LblPtr->m_textRect.w / 2);
	int multiplicator2LblPosY = (m_windowPtr->windHeight / 2) - (m_multiplicator2LblPtr->m_textRect.h / 2);
	m_multiplicator2LblPtr->setTextPosition(multiplicator2LblPosX, multiplicator2LblPosY);

	m_multiplicator3LblPtr = std::make_unique <Label>("! x3 !", m_labelDftColor, m_windowPtr->m_rendererPtr, "arial.ttf", 60);
	int multiplicator3LblPosX = (m_windowPtr->gameFieldWidth / 2) - (m_multiplicator3LblPtr->m_textRect.w / 2);
	int multiplicator3LblPosY = (m_windowPtr->windHeight / 2) - (m_multiplicator3LblPtr->m_textRect.h / 2);
	m_multiplicator3LblPtr->setTextPosition(multiplicator3LblPosX, multiplicator3LblPosY);

	m_multiplicator4LblPtr = std::make_unique <Label>("! x4 !", m_labelDftColor, m_windowPtr->m_rendererPtr, "arial.ttf", 70);
	int multiplicator4LblPosx = (m_windowPtr->gameFieldWidth / 2) - (m_multiplicator4LblPtr->m_textRect.w / 2);
	int multiplicator4LblPosY = (m_windowPtr->windHeight / 2) - (m_multiplicator4LblPtr->m_textRect.h / 2);
	m_multiplicator4LblPtr->setTextPosition(multiplicator4LblPosx, multiplicator4LblPosY);
}

void GameFlow::changeGameSpeed() 
{
	switch (m_speedMultiplier)
	{
	case 0:
		m_speedMultiplier = GameSpeed::normal;
		break;
	case 1:
		m_speedMultiplier = GameSpeed::fast;
		break;
	case 2:
		m_speedMultiplier = GameSpeed::slow;
		break;
	default:
		break;
	}
	m_gameMenuPtr->changeSpeed(m_speedMultiplier);
}

void GameFlow::eventHandler(SDL_Event &event) 
//TODO separate class event handler in order to move this funtionality out of this class
{
	while (SDL_PollEvent(&event)) 
	{
		switch (event.type)
		{
		case SDL_QUIT:
			m_windowPtr->windowClosed = true;
			break;
		case SDL_MOUSEBUTTONDOWN:
			if (event.button.button == SDL_BUTTON_LEFT)
			{
				switch (m_gameState)
				{
				case GameState::initialized:
					if (isMouseOver(m_gameMenuPtr->m_buttonsList[GameButton::startBtn].get()))
					{
						m_gameState = GameState::started;
						m_gameMenuPtr->m_isMenuPresented = false;
					}
					else if (isMouseOver(m_gameMenuPtr->m_buttonsList[GameButton::chanSpeedBtn].get()))
					{
						changeGameSpeed();
					}
					else if (isMouseOver(m_gameMenuPtr->m_buttonsList[GameButton::playerName].get()))
					{
						m_gameState = GameState::inputText;
						m_gameMenuPtr->setInputTextMenuState();
					}
					else if (isMouseOver(m_gameMenuPtr->m_buttonsList[GameButton::exitBtn].get()))
					{
						m_windowPtr->windowClosed = true;
					}
					else if (isMouseOver(m_gameMenuPtr->m_buttonsList[GameButton::scoreListBtn].get()))
					{
						m_gameState = GameState::scoreScreenMenu;
					}

					break;
				case GameState::paused:
					if (isMouseOver(m_gameMenuPtr->m_buttonsList[GameButton::startBtn].get()))
					{
						m_gameState = GameState::started;
						m_gameMenuPtr->m_isMenuPresented = false;

					}
					else if (isMouseOver(m_gameMenuPtr->m_buttonsList[GameButton::restartBtn].get()))
					{
						m_currentShapePtr = nullptr;
						restartGame();
						m_gameState = GameState::started;
						m_gameMenuPtr->m_isMenuPresented = false;
					}
					else if (isMouseOver(m_gameMenuPtr->m_buttonsList[GameButton::exitBtn].get()))
					{
						m_windowPtr->windowClosed = true;
					}
					break;
				case GameState::inputText:
					if (!isMouseOver(m_gameMenuPtr->m_buttonsList[GameButton::playerName].get()))
					{
						m_gameState = GameState::initialized;
						m_gameMenuPtr->setRegularMenuState();
					}
					break;
				default:
					break;
				}
			}
			break;

		case SDL_KEYDOWN:
			switch (m_gameState)
			{
			case GameState::inputText:
			{
				bool textChanged = false;
				if (event.key.keysym.sym == SDLK_BACKSPACE)
				{
					if (m_gameMenuPtr->m_playerName.length() > 0)
					{
						m_gameMenuPtr->m_playerName.pop_back();
						textChanged = true;
					}
				}
				else if (event.key.keysym.sym == SDLK_RETURN)
				{
					m_gameState = GameState::initialized;
					m_gameMenuPtr->setRegularMenuState();
				}
				else
				{
					if (m_gameMenuPtr->m_playerName.length() < 15)
					{
						if (event.key.keysym.sym > 32 && event.key.keysym.sym < 127)
						{
							m_gameMenuPtr->m_playerName += SDL_GetKeyName(event.key.keysym.sym);
							textChanged = true;
						}
						else if (event.key.keysym.sym == SDLK_SPACE && m_gameMenuPtr->m_playerName.length() > 0)
						{
							m_gameMenuPtr->m_playerName += " ";
							textChanged = true;
						}
					}
				}
				if (textChanged)
				{
					m_gameMenuPtr->m_buttonsList[GameButton::playerName]->changeBtnText(" " + m_gameMenuPtr->m_playerName, true);
					int newPromptPosX = m_gameMenuPtr->m_buttonsList[GameButton::playerName]->m_buttonArea.x + m_gameMenuPtr->m_buttonsList[GameButton::playerName]->m_buttonArea.w + 3;
					int newPromptPosY = m_gameMenuPtr->m_buttonsList[GameButton::playerName]->m_buttonArea.y;
					m_gameMenuPtr->m_txtInputPromptPtr->setTextPosition(newPromptPosX, newPromptPosY);
				}
				break;
			}
			case GameState::started:
				//currently buttons are hardcoded, there is no functionality to get custom 
				//buttons setting from settings.xml (mostly due to button's name which is enum)
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					m_gameMenuPtr->m_isMenuPresented = true;
					m_gameState = GameState::paused;
					m_gameMenuPtr->setPausedGameMenuState();
					break;
				case SDLK_RIGHT:
					if (m_currentShapePtr != nullptr && !m_gameMenuPtr->m_isMenuPresented && !m_currentShapePtr->m_blockedShape)
					{
						m_currentShapePtr->move(Direction::right);
					}
					break;
				case SDLK_LEFT:
					if (m_currentShapePtr != nullptr && !m_gameMenuPtr->m_isMenuPresented && !m_currentShapePtr->m_blockedShape)
					{
						m_currentShapePtr->move(Direction::left);
					}
					break;
				case SDLK_UP:
					if (m_currentShapePtr != nullptr && !m_gameMenuPtr->m_isMenuPresented && !m_currentShapePtr->m_blockedShape)
					{
						m_currentShapePtr->rotate();
					}
					break;
				case SDLK_DOWN:
					if (m_currentShapePtr != nullptr && !m_gameMenuPtr->m_isMenuPresented && !m_currentShapePtr->m_blockedShape)
					{
						m_currentShapePtr->move(Direction::down);
					}
					break;
				default:
					break;
				}
				break;
			case GameState::scoreScreenAfterGame:
				if (event.key.keysym.sym == SDLK_RETURN)
				{
					reinitGame();
				}
				break;
			case GameState::scoreScreenMenu:
				if (event.key.keysym.sym == SDLK_RETURN)
				{
					m_gameState = GameState::initialized;
				}
				break;
			default:
				break;
			}
		default:
			break;
		}
	}
}

void GameFlow::drawGame()
{
	drawSplitLine();
	drawSideField();
	drawLabelsAndButtons();
	drawCurrentGame(m_matrixPtr->m_matrix);
	if (m_gameState == GameState::lineErasing)
	{
		switch (m_erasedLines)
		{
		case 1:
			m_multiplicator1LblPtr->renderText();
			break;
		case 2:
			m_multiplicator2LblPtr->renderText();
			break;
		case 3:
			m_multiplicator3LblPtr->renderText();
			break;
		case 4:
			m_multiplicator4LblPtr->renderText();
			break;
		default:
			break;
		}
	}
}

void GameFlow::drawScoreScreen()
{
	m_scoreMgrPtr->drawScoreScreen();
}

void GameFlow::restartGame() 
{
	m_gameOver = false;
	m_matrixPtr->cleanMatrix();
	resetScore();
}

void GameFlow::reinitGame()
{
	m_gameState = GameState::initialized;
	m_gameMenuPtr->showMenu();
	resetScore();
	m_currentShapePtr = nullptr;
	m_nextShapePtr = nullptr;
	restartGame();
}

bool GameFlow::canPlayCurrentGame()
{
	if (m_matrixPtr->getFieldType(8, 0) == FieldType::blocked ||
		m_matrixPtr->getFieldType(9, 0) == FieldType::blocked)
	{

		return false;
	}
	else
	{
		return true;
	}
}

void GameFlow::finishCurrentGame()
{
	m_gameState = GameState::scoreScreenAfterGame;
	m_gameMenuPtr->setRegularMenuState();
	m_gameOver = true;
}

void GameFlow::drawMenu() 
{
	m_gameMenuPtr->renderButtons();
	static bool showPrompt = true;
	if (m_gameState == GameState::inputText)
	{
		m_promptTimerPtr->getTime();
		if (m_promptTimerPtr->isNextFrame(300))
		{
			showPrompt = !showPrompt;
		}
		if (showPrompt)
		{
			m_gameMenuPtr->m_txtInputPromptPtr->renderText();
		}
	}
}

void GameFlow::drawSideField()
{
	SDL_SetRenderDrawColor(m_windowPtr->m_rendererPtr,
							m_nextShapePtr->getColorRed(),
							m_nextShapePtr->getColorGreen(),
							m_nextShapePtr->getColorBlue(), 255);
	for (auto i = m_nextShapePtr->m_shapePreviewRects.begin(); i < m_nextShapePtr->m_shapePreviewRects.end(); i++)
	{
		SDL_RenderFillRect(m_windowPtr->m_rendererPtr, &(*i));
	}
	 
}

void GameFlow::drawLabelsAndButtons()
{
	m_nextShapeLblPtr->renderText();
	m_scoreLblPtr->renderText();
	m_scoreCounterLblPtr->renderText();
	m_gameMenuPtr->m_chngPlayerNameLblPtr->renderText();
	m_gameMenuPtr->m_buttonsList[GameButton::playerName]->renderBtnText();
}

void GameFlow::drawCurrentGame(std::vector<std::vector<Rectangle>> &matrix)
{
	for (int i = 0; i < matrix.size(); i++)
	{
		for (int j = 0; j < matrix[i].size(); j++)
		{
			if (matrix[i][j].m_rectType != FieldType::empty)
			{
				SDL_SetRenderDrawColor(m_windowPtr->m_rendererPtr, matrix[i][j].m_rectColor.r,
					matrix[i][j].m_rectColor.g,
					matrix[i][j].m_rectColor.b, 255);
				SDL_RenderFillRect(m_windowPtr->m_rendererPtr, &matrix[i][j]);
			}
		}
	}
}

void GameFlow::drawSplitLine() const
{
	SDL_SetRenderDrawColor(m_windowPtr->m_rendererPtr, 255, 255, 255, 255);
	SDL_RenderDrawLine(m_windowPtr->m_rendererPtr, m_windowPtr->gameFieldWidth + 1, 0, m_windowPtr->gameFieldWidth + 1, m_windowPtr->windHeight);
}

std::unique_ptr <Shape> GameFlow::initShape(int number, bool preview)
{
	switch (number)
	{
	case 0:
	{
		std::unique_ptr <Shape> shapePtr = std::make_unique <Lshape>(m_matrixPtr.get(), preview);
		return shapePtr;
	}
	case 1:
	{
		std::unique_ptr <Shape> shapePtr = std::make_unique <Rshape>(m_matrixPtr.get(), preview);
		return shapePtr;
	}
	case 2:
	{
		std::unique_ptr <Shape> shapePtr = std::make_unique <Ishape>(m_matrixPtr.get(), preview);
		return shapePtr;
	}
	case 3:
	{
		std::unique_ptr <Shape> shapePtr = std::make_unique <Jshape>(m_matrixPtr.get(), preview);
		return shapePtr;
	}
	case 4:
	{
		std::unique_ptr <Shape> shapePtr = std::make_unique <Zshape>(m_matrixPtr.get(), preview);
		return shapePtr;
	}
	case 5:
	{
		std::unique_ptr <Shape> shapePtr = std::make_unique <Sshape>(m_matrixPtr.get(), preview);
		return shapePtr;
	}
	case 6:
	{
		std::unique_ptr <Shape> shapePtr = std::make_unique <Tshape>(m_matrixPtr.get(), preview);
		return shapePtr;
	}
	default:
		break;
	}
}

int GameFlow::randomNumber() 
{
	srand(time(NULL));
	int random = rand() % 7;
	return random;
}

int GameFlow::countScore(int lines)
{
	int tmpScore = 0;
	tmpScore += m_oneLineScore * lines * lines * (m_speedMultiplier + 1);
	return tmpScore;
}

void GameFlow::resetScore()
{
	m_scoreMgrPtr->resetScore();
	m_scoreCounterLblPtr->changeRenderedText(std::to_string(m_scoreMgrPtr->getCurrentScore()));
	m_scoreCounterLblPtr->setTextPosition((m_windowPtr->gameFieldWidth) + (m_windowPtr->sideFieldWidth/2)-(m_scoreCounterLblPtr->m_textRect.w/2), m_scoreCounterLblPtr->m_textRect.y);
}

void GameFlow::increaseScore(int score)
{
	m_scoreMgrPtr->increaseScore(countScore(score));
	m_scoreCounterLblPtr->changeRenderedText(std::to_string(m_scoreMgrPtr->getCurrentScore()));
	m_scoreCounterLblPtr->setTextPosition((m_windowPtr->gameFieldWidth) + (m_windowPtr->sideFieldWidth / 2) - (m_scoreCounterLblPtr->m_textRect.w / 2), m_scoreCounterLblPtr->m_textRect.y);
}

bool GameFlow::isMouseOver(Button *button)
{
	int mousePosX, mousePosY;
	SDL_GetMouseState(&mousePosX, &mousePosY);

	if(mousePosX > button->m_buttonArea.x  &&
		mousePosX < (button->m_buttonArea.x) + (button->m_buttonArea.w) &&
		mousePosY > button->m_buttonArea.y &&
		mousePosY < (button->m_buttonArea.y) + (button->m_buttonArea.h))
	{
		return true;
	}
	else
	{
		return false;
	}
}
