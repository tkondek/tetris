#pragma once
#include "stdafx.h"
#include "label.h"
#include "timer.h"
#include "settings.h"

class ScoreManager
{
private:
	std::unique_ptr <Timer> timer;
	int scoresAmount = 6;
	int m_currentScore = 0;
	int m_tempScore = 0;
	SDL_Renderer *m_renderer;
	SDL_Color scoreFontColor = { 46, 55, 71 };
	SDL_Color backgroundColor = { 177, 196, 226 };
	SDL_Rect scoreBackgroundRect;
	const int backgroundWidth = 350;
	const int backgroundHeight = 400;
	int backgroundPosX;
	int backgroundPosY;
	std::vector <std::pair <std::string, std::string>> scoreList;
	std::vector <std::unique_ptr <Label>> scoreLabelsList;
	std::unique_ptr <Label> scoreListHeaderPtr;
	std::unique_ptr <Label> continueLblPtr;
	void initScoreList();
	void initScoreListLabels();
	std::pair<std::string, std::string> getPlayerAndScoreFromTxt(std::string line);
	void updateScoreScreen();
	void increaseTempScore(int value);

public:
	ScoreManager(SDL_Renderer *renderer, Settings *settings);
	inline int getCurrentScore() { return m_currentScore; }
	inline int getTempScore() { return m_tempScore; }
	void increaseScore(int value);
	void resetScore();
	void drawScoreScreen();
	void checkAndSaveScore(std::string player, int score);


};