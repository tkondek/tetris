 #include "window.h"
 #include "gameFlow.h"
 #include "settings.h"	



int main(int argc, char* args[])
{
	std::shared_ptr<Settings> settingsPtr = std::make_shared<Settings>();
	Window window("Tetris v1.0", settingsPtr.get());
	GameFlow game(&window, settingsPtr.get());
	game.playGame();
	return 0;
}