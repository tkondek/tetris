#pragma once

#include "matrix.h"
#include "shape.h"
#include "window.h"
#include "gameMenu.h"
#include "timer.h"
#include "scoreManager.h"

enum class GameState:char { initialized, 
							started, 
							lineErasing,
							paused, 
							finished,
							inputText,
							scoreScreenMenu,
							scoreScreenAfterGame};
enum GameSpeed {slow = 0,
				normal = 1, 
				fast = 2};
enum class GameButton:char {startBtn, 
							restartBtn, 
							playerName,
							chanSpeedBtn, 
							exitBtn,
							scoreListBtn};

class GameFlow {
	
private:
	bool m_gameOver = false;
	GameState m_gameState = GameState::initialized;
	GameSpeed m_speedMultiplier;
	const int m_shapesAmount = 7; //in standard tetris there is 7 shapes available
	const int m_oneLineScore = 10;
	int m_currentGameSpeed = 300; //in miliseconds, delay to next shape move
	int m_erasedLines = 0;
	Window *m_windowPtr;
	std::unique_ptr <GameMenu> m_gameMenuPtr = nullptr;
	std::shared_ptr <Matrix> m_matrixPtr = nullptr;
	std::unique_ptr <ScoreManager> m_scoreMgrPtr = nullptr;
	std::unique_ptr <Shape> m_currentShapePtr = nullptr;
	std::unique_ptr <Shape> m_nextShapePtr = nullptr;
	std::unique_ptr <Label> m_nextShapeLblPtr = nullptr;
	std::unique_ptr <Label> m_scoreLblPtr = nullptr;
	std::unique_ptr <Label> m_scoreCounterLblPtr = nullptr;
	std::unique_ptr <Label> m_multiplicator1LblPtr = nullptr;
	std::unique_ptr <Label> m_multiplicator2LblPtr = nullptr;
	std::unique_ptr <Label> m_multiplicator3LblPtr = nullptr;
	std::unique_ptr <Label> m_multiplicator4LblPtr = nullptr;
	std::unique_ptr <Timer> m_gameTimerPtr;
	std::unique_ptr <Timer> m_promptTimerPtr;
	SDL_Color m_labelDftColor = { 255,255,255 };
	std::unique_ptr <Shape> initShape(int number, bool preview = true);
	void eventHandler(SDL_Event &event);
	void initLabels();
	void drawScoreScreen();
	void restartGame();
	void reinitGame();
	bool canPlayCurrentGame();
	void finishCurrentGame();
	void drawSplitLine() const;
	void drawSideField();
	void drawLabelsAndButtons();
	void drawCurrentGame(std::vector<std::vector<Rectangle>> &matrix);
	int randomNumber();
	int countScore(int lines);
	void resetScore();
	void increaseScore(int score);
	bool isMouseOver(Button *button);
	void changeGameSpeed();
	void drawGame();
	void drawMenu();
	
public:
	GameFlow(Window *window, Settings *settings);
	void playGame();
};