#pragma once
#include "rectangle.h"
#include "shape.h"

enum class FieldType : char { empty, symOccupied, blocked };
struct ShapeRectPos;

class Matrix {

private:
	const int m_matrixWidth = 16;
	const int m_matrixHeight = 24;

public:
	Matrix();
	std::vector <std::vector <Rectangle>> m_matrix;
	FieldType getFieldType(int posX, int posY);
	void fillMatrix(int posX, int posY, FieldType type, SDL_Color rectColor = {0 , 0, 0});
	void fillMatrix(ShapeRectPos rectanglesPositions, FieldType type, SDL_Color rectColor = {0 , 0, 0});
	void redrawMatrix();
	void initMatrix();
	void cleanMatrix();
	bool eraseFullLines();
};