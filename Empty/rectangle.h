#pragma once
#include "stdafx.h"

enum class FieldType : char;

struct Rectangle:public SDL_Rect {
	Rectangle();
	Rectangle(FieldType fieldType, SDL_Color rectColor);
	void setPosition(int posX, int posY, int width, int height);
	SDL_Color m_rectColor;
	FieldType m_rectType;
};

