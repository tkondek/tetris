#pragma once
#include "shape.h"

class Sshape :public Shape {

public:
	Sshape(Matrix *matrix, bool m_preview);
	~Sshape();
	void initShapePreview();
	void initRegularShape();
	virtual void rotate();
};