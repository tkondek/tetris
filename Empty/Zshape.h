#pragma once
#include "shape.h"

class Zshape :public Shape {

public:
	Zshape(Matrix *matrix, bool m_preview);
	~Zshape();
	void initShapePreview();
	void initRegularShape();
	virtual void rotate();
};