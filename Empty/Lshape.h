#pragma once
#include "shape.h"

class Lshape:public Shape{

public:	
	Lshape(Matrix *matrix, bool m_preview);
	~Lshape();
	void initShapePreview();
	void initRegularShape();
	virtual void rotate();
};