#pragma once
#include "stdafx.h"

class Label {
private:
	std::string m_fontFile;
	int m_fontSize;
	std::string m_text;
	SDL_Renderer *m_rendererPtr;
	TTF_Font* m_font = nullptr;
	SDL_Surface *m_textSurface;
	SDL_Texture *m_textTexture;
	bool loadFont();
	void createTextTexture();
	
public:
	Label(std::string text,
				SDL_Color textColor,
				SDL_Renderer *renderer,
				std::string fontFile = "arial.ttf",
				int fontSize = 14);
	
	~Label();
	SDL_Rect m_textRect;
	SDL_Color m_textColor;
	void setTextPosition(int posX = 0, int posY = 0);
	void renderText();
	void changeRenderedText(std::string newText);
	void changeTextColor(SDL_Color newColor);
	
};
