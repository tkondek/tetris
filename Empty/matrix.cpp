#include "matrix.h"


Matrix::Matrix() 
{
	initMatrix();
}

void Matrix::initMatrix() 
{
	for (unsigned i = 0; i < m_matrixHeight; i++) 
	{
		std::vector <Rectangle> tempVect;
		for (unsigned j = 0; j < m_matrixWidth; j++) 
		{
			SDL_Color rectColor = { 0,0,0 };
			Rectangle rectangle(FieldType::empty, rectColor);
			rectangle.setPosition(j * 25, i * 25, 24, 24);
			tempVect.push_back(rectangle);
		}
		m_matrix.push_back(tempVect);
	}
} 

void Matrix::cleanMatrix()
{
	for (unsigned i = 0; i < m_matrixHeight; i++)
	{
		for (unsigned j = 0; j < m_matrixWidth; j++)
		{
			m_matrix[i][j].m_rectType = FieldType::empty;
		}
	}
}

void Matrix::redrawMatrix() 
{
	system("cls");
	for (std::vector <std::vector<Rectangle>>::iterator i = m_matrix.begin(); i < m_matrix.end(); i++)
	{
		std::cout << "|";
		for (auto j = (*i).begin(); j < (*i).end(); j++) 
		{
			std::cout << char((*j).m_rectType);
		}
		std::cout << "|" << std::endl;
	}
}

FieldType Matrix::getFieldType(int posX, int posY) 
{
	if (posX >= 0 && posY >= 0) 
	{
		return m_matrix[posY][posX].m_rectType;
	}
}

void Matrix::fillMatrix(int posX, int posY, FieldType type, SDL_Color rectColor) 
{
	if (posX >= 0 && posY >= 0) 
	{
		m_matrix[posY][posX].m_rectType = type;
		m_matrix[posY][posX].m_rectColor = rectColor;

	}
}

void Matrix::fillMatrix(ShapeRectPos rectanglesPositions, FieldType type, SDL_Color rectColor)
{
	// shape is initialized above game field, matrix can not be filled outside it's range
	if (rectanglesPositions.rect1PositionY >= 0)
	{
		m_matrix[rectanglesPositions.rect1PositionY][rectanglesPositions.rect1PositionX].m_rectType = type;
		m_matrix[rectanglesPositions.rect1PositionY][rectanglesPositions.rect1PositionX].m_rectColor = rectColor;
	}
	if (rectanglesPositions.rect2PositionY >= 0)
	{
		m_matrix[rectanglesPositions.rect2PositionY][rectanglesPositions.rect2PositionX].m_rectType = type;
		m_matrix[rectanglesPositions.rect2PositionY][rectanglesPositions.rect2PositionX].m_rectColor = rectColor;
	}
	if (rectanglesPositions.rect3PositionY >= 0)
	{
		m_matrix[rectanglesPositions.rect3PositionY][rectanglesPositions.rect3PositionX].m_rectType = type;
		m_matrix[rectanglesPositions.rect3PositionY][rectanglesPositions.rect3PositionX].m_rectColor = rectColor;
	}
	if (rectanglesPositions.rect4PositionY >= 0)
	{
		m_matrix[rectanglesPositions.rect4PositionY][rectanglesPositions.rect4PositionX].m_rectType = type;
		m_matrix[rectanglesPositions.rect4PositionY][rectanglesPositions.rect4PositionX].m_rectColor = rectColor;
	}
}

bool Matrix::eraseFullLines()
{
	int lineErased = false;
	for (int i = 0; i < 24; i++)
	{
		int blockedFields = 0;
		for (int j = 0; j < 16; j++)
		{
			if (m_matrix[i][j].m_rectType == FieldType::blocked)
			{
				blockedFields++;
			}
		}
		if (blockedFields == 16)
		{
			lineErased = true;
			for (int m = 0; m < 16; m++)
			{
				m_matrix[i][m].m_rectType = FieldType::empty;
			}
			for (int k = i - 1; k > 0; k--)
			{
				for (int l = 0; l < 16; l++)
				{
					SDL_Color currentRectColor = { 0,0,0 };
					if (m_matrix[k][l].m_rectType == FieldType::blocked)
					{
						currentRectColor = m_matrix[k][l].m_rectColor;

						m_matrix[k][l].m_rectType = FieldType::empty;
						m_matrix[k + 1][l].m_rectType = FieldType::blocked;
						m_matrix[k + 1][l].m_rectColor = currentRectColor;
					}

				}
			}
			break;
		}
	}
	return lineErased;
}