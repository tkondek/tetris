#pragma once
#include "stdafx.h"
#include "boost\property_tree\xml_parser.hpp"
#include "boost\property_tree\ptree.hpp"

class Settings
{
private:
	const std::string XML_NAME = "settings.xml";
	void readXml(const std::string &settingsXml);
	void createDefaultXml();
	boost::property_tree::ptree xmlTree;
	const std::string pauseKey = "pauseKey";
	const std::string leftKey = "leftKey";
	const std::string rightKey = "rightKey";
	const std::string downKey = "downKey";
	const std::string rotateKey = "rotateKey";
	const std::string windWidth = "windWidth";
	const std::string windHeight = "windHeight";
	const std::string gameSpeed = "gameSpeed";
	std::unordered_map <std::string, std::string> settingsList;

public:
	Settings();
	~Settings();
	int getSpeedMultiplier();
	int getWindWidth();
	int getWindHeight();
};


//TODO 'GAME SETTINGS' section in gameMenu for parameters editing