#pragma once
#include "shape.h"

class Tshape :public Shape {

public:
	Tshape(Matrix *matrix, bool m_preview);
	~Tshape();
	void initShapePreview();
	void initRegularShape();
	virtual void rotate();
};