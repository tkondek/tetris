#pragma once
#include <SDL.h>


class Timer {
private:
	Uint32 startTime;
	Uint32 currentTime;
public:
	bool isNextFrame(int ticksDelay);
	Uint32 setStartTime();
	Uint32 getTime();
};