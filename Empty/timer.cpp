#include "timer.h"
#include "stdafx.h"


Uint32 Timer::setStartTime() 
{
	startTime = SDL_GetTicks();
	return startTime;
}

Uint32 Timer::getTime() 
{
	currentTime = SDL_GetTicks();
	return currentTime;
}

bool Timer::isNextFrame(int ticksDelay) 
{
	if ((currentTime - startTime) > ticksDelay) 
	{
		startTime = currentTime;
		return true;
	}
	else 
	{
		return false;
	}
}